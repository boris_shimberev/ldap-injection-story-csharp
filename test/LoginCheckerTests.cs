﻿using LdapInjectionStory;
using NUnit.Framework;

namespace LdapInjectionStoryTests
{
    [TestFixture]
    public class LoginCheckerTests
    {
        [Test]
        public void Check_ValidLogin_True()
        {
            var directoryContextMock = LdapConnectorMock.GetDirectoryContextMock(
                resultQueryable: LdapConnectorMock.GetOnlyLoginUser());
            var ldapConnector = LdapConnectorMock.GetLdapConnector(directoryContextMock);
            var loginChecker = new LoginChecker(ldapConnector);
            Assert.IsTrue(loginChecker.Check(LdapConnectorMock.DummyLogin));
        }

        [Test]
        public void Check_InvalidLogin_False()
        {
            var directoryContextMock = LdapConnectorMock.GetDirectoryContextMock(
                resultQueryable: LdapConnectorMock.GetEmptyUsers());
            var ldapConnector = LdapConnectorMock.GetLdapConnector(directoryContextMock);
            var loginChecker = new LoginChecker(ldapConnector);
            Assert.IsFalse(loginChecker.Check(LdapConnectorMock.DummyLogin));
        }

        [Test]
        public void Check_TwoUsersWithSameLogin_True()
        {
            var directoryContextMock = LdapConnectorMock.GetDirectoryContextMock(
                resultQueryable: LdapConnectorMock.GetRepeatingUsers());
            var ldapConnector = LdapConnectorMock.GetLdapConnector(directoryContextMock);
            var loginChecker = new LoginChecker(ldapConnector);
            Assert.IsTrue(loginChecker.Check(LdapConnectorMock.DummyLogin));
        }
    }
}