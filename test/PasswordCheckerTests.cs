﻿using System;
using LdapInjectionStory;
using NUnit.Framework;

namespace LdapInjectionStoryTests
{
    [TestFixture]
    public class PasswordCheckerTests
    {
        [Test]
        public void Check_ValidPassword_True()
        {
            var directoryContextMock = LdapConnectorMock.GetDirectoryContextMock(
                resultQueryable: LdapConnectorMock.GetValidUser());
            var ldapConnector = LdapConnectorMock.GetLdapConnector(directoryContextMock);
            var passwordChecker = new PasswordChecker(ldapConnector);
            Assert.IsTrue(
                passwordChecker.Check(
                    LdapConnectorMock.DummyLogin,
                    LdapConnectorMock.DummyPassword));
        }

        [Test]
        public void Check_InvalidPassword_False()
        {
            var directoryContextMock = LdapConnectorMock.GetDirectoryContextMock(
                resultQueryable: LdapConnectorMock.GetOnlyLoginUser());
            var ldapConnector = LdapConnectorMock.GetLdapConnector(directoryContextMock);
            var passwordChecker = new PasswordChecker(ldapConnector);
            Assert.IsFalse(
                passwordChecker.Check(
                    LdapConnectorMock.DummyLogin,
                    LdapConnectorMock.DummyPassword));
        }

        [Test]
        public void Check_RepeatingUsersPassword_Throws()
        {
            var directoryContextMock = LdapConnectorMock.GetDirectoryContextMock(
                resultQueryable: LdapConnectorMock.GetRepeatingUsers());
            var ldapConnector = LdapConnectorMock.GetLdapConnector(directoryContextMock);
            var passwordChecker = new PasswordChecker(ldapConnector);
            Assert.Throws<InvalidOperationException>(
                () => passwordChecker.Check(
                    LdapConnectorMock.DummyLogin,
                    LdapConnectorMock.DummyPassword));
        }
    }
}