﻿using System;
using System.DirectoryServices.Protocols;
using System.Net;
using LinqToLdap;

namespace LdapInjectionStory.LdapConnection
{
    public interface ILdapConnector
    {
        IDirectoryContext GetConnection();
    }

    public class LdapConnector : ILdapConnector
    {
        private const string  AdServer = "127.0.0.1";
        private const int  AdPort = 1389;
        private const string  AdUser = @"cn=admin,dc=example,dc=org";
        private const string  AdPassword = "adminpassword";
        private static readonly Lazy<LdapConfiguration> LdapConfiguration
            = new(GetLdapConfiguration);
        private static readonly NetworkCredential NetworkCredential
            = new(AdUser, AdPassword);

        public IDirectoryContext GetConnection() => new DirectoryContext(LdapConfiguration.Value);

        private static LdapConfiguration GetLdapConfiguration()
        {
            var config = new LdapConfiguration()
                .MaxPageSizeIs(5)
                .AddMapping(new UserMapping());

            config
                .ConfigureFactory(AdServer)
                .UsePort(AdPort)
                .AuthenticateAs(NetworkCredential)
                .AuthenticateBy(AuthType.Basic)
                .ProtocolVersion(3);

            return config.UseStaticStorage();
        }
    }
}