﻿using System;
using LdapInjectionStory;
using LdapInjectionStory.LdapConnection;

var loginChecker = new LoginChecker(new LdapConnector());
var passwordChecker = new PasswordChecker(new LdapConnector());

while (TryPromptAndRepeat())
{
}

bool TryPromptAndRepeat()
{
    if (!TryPromptLogin(out var login))
    {
        return false;
    }

    if (TryPromptPassword(login))
    {
        return false;
    }

    Console.Write("Password is incorrect! Shall we do this again (y/n)? ");

    var yesNo = Console.ReadLine();

    return yesNo.ToLower() == "y" ;
}

bool TryPromptLogin(out string login)
{
    Console.Write("Enter login: ");

    // login = "user02";
    login = Console.ReadLine();

    var loginExists = loginChecker.Check(login);

    if (!loginExists)
    {
        Console.Write($"No logins are found, goodbye, {login}");
    }

    return loginExists;
}

bool TryPromptPassword(string login)
{
    Console.Write($"Ok, {login}, enter your password: ");

    // var password = bitnami2;
    var password = Console.ReadLine();

    var passwordIsOk = passwordChecker.Check(login, password);

    if (passwordIsOk)
    {
        Console.Write("Password is correct! Goodbye!");
    }

    return passwordIsOk;
}